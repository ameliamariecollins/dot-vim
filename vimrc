set encoding=utf-8
scriptencoding utf-8

if has('eval')
    let g:conf = get(g:, 'conf', {}) " Ensure presence of conf dictionary

    " Although these value can be set here, setting them in ~/.vimrc.local
    " will avoid conflicts with updates to this file via version control,
    " and also offers per-host customization. Values in the local
    " configuration override values listed here.
    "
    "  ┌───────────────┐
    "──┤ configuration ├──────────────────────────────────────────────────────

    " COLOR SCHEME: The 'helix' color scheme is used by default. If you
    " prefer another, set it here.
    " Default: 'helix'
    "let g:conf.color_scheme = 'helix'

    " POSH FONT: If you wish to use the extended character set for LightLine
    " and other operations, you may set this option. In addition, this will
    " download the font automatically if /usr/bin/fc-cache is present.
    " Default: 0
    "let g:conf.posh_font = 1

    " PERSISTENT UNDO: Vim will store undos alongside the edited files (in
    " .filename.ext.un~).  If you prefer instead to store them elsewhere,
    " set this option.
    " Default: (unset)
    "let g:conf.vim_undo_dir = '~/.vim-undo'

    "─────────────────────────────────────────────────────────────────────────

endif

runtime! vimrc.d/*.vim

