" if has('vim9script")
"     vim9script noclear
" 
"     if exists('loaded') | finish | endif
"     var loaded = true
" 
"     import autoload '../autoload/rgb2name/rgb2name.vim'
" 
"     command -bar -nargs=1 Rgb2name echom rgb2name#rgb2name#Rgb2name(<q-args>)
"     command -bar -nargs=1 RGBClosest rgb2name#rgb2name#RGBClosest(<q-args>)
"     map <leader>rn :call rgb2name#rgb2name#NameThisColor()<CR>
" endif
