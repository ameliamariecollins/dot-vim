vim9script
scriptencoding utf-8

var rgb_map: dict<list<number>>
var reverse_map: dict<string>

def Hex2Dec(hex: string): number
    return str2nr(hex, 16)
enddef

def Hex2RGB(hex: string): list<number>
    return [Hex2Dec(hex[0 : 1]), Hex2Dec(hex[2 : 3]), Hex2Dec(hex[4 : 5])]
enddef

def BuildRGBMap()
    BuildReverseMap()
    if !!rgb_map | return | endif
    for [hex, name] in items(reverse_map)
	rgb_map[name] = Hex2RGB(hex)
    endfor
enddef

export def RGBClosest(hex: string): string
    BuildReverseMap()
    BuildRGBMap()
    var point_a_rgb = Hex2RGB(hex)

    var best_color_name: string
    var best_color_hex: string
    var best_color_badness = 500.0

    for [this_hex, this_name] in items(reverse_map)
	var point_b_rgb = rgb_map[this_name]
	var sq_total: float
	for i in [0, 1, 2]
	    sq_total += pow(point_a_rgb[i] - point_b_rgb[i], 2)
	endfor
	var this_badness = sqrt(sq_total)
	if this_badness < best_color_badness
	    best_color_name = this_name
	    best_color_hex = this_hex
	    best_color_badness = this_badness
	endif
    endfor

    var badness_max = sqrt(3 * pow(255, 2))
    echom "Closest color: " .. best_color_name
	.. "(#" .. best_color_hex .. ") "
	.. "Badness: " .. best_color_badness / badness_max

    return best_color_name
enddef

def BuildReverseMap()
    if !!reverse_map | return | endif
    # Make a list of ALL names for a given color code
    var all_names = {}
    for [name, hex] in items(v:colornames)  # They are "ColorName", "#xxxxxx"
	var h = hex[1 :]
	if !has_key(all_names, h)
	    all_names[h] = []
	endif
	add(all_names[h], name)
    endfor

    # Cherry-pick name with best qualities
    for [hex, names] in items(all_names)
	# Optimal: lower case, no space
	for name in names
	    if name =~ '[\l\d]\{6}'
		reverse_map[hex] = name
		break
	    endif
	endfor
	# Non-optimal: No space, but mixed-case
	for name in names
	    if name !~ ' '
		reverse_map[hex] = name
		break
	    endif
	endfor
	# Pessimal: Accept any remaining choice
	reverse_map[hex] = names[0]
    endfor
enddef

# a9a9a9
# 0000fd
export def NameThisColor()
    var hex = expand("<cword>")
    echom Rgb2name(hex)
enddef

export def Rgb2name(rgb: string): string
    BuildReverseMap()
    const r = substitute(rgb, '#', '', '')  # Remove hash prefix
     if has_key(reverse_map, r)
 	return reverse_map[r]
     endif
    return "Not Found"
enddef

defcompile

