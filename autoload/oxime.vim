scriptencoding utf-8
"   ______   __   __   _    _________    ______ 
"  / |  | \  \ \_/ /  | |  | | | | | \  | |     
"  | |  | |   - _ -   | |  | | | | | |  | |---- 
"  \_|__|_/  /_/ \_\  |_|  |_| |_| |_|  |_|____ 
"

function oxime#init()
endfunction

function oxime#ensure_dir(dir)
    let l:dir = expand(a:dir)
    if !isdirectory(l:dir)
	call mkdir(l:dir, 'p')
    endif
endfunction

function oxime#colorscheme_refresh(...)
    if (a:0 > 0)
	execute 'colorscheme' a:1
    else
	if exists('g:colors_name')
	    execute 'colorscheme' g:colors_name
	endif
    endif

    if exists('g:lightline')
	call lightline#colorscheme()
    endif
endfunction


