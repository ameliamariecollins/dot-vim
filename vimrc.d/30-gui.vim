scriptencoding utf-8

if !has('gui_running')
    finish
endif

set guifont=Monaco:h20
"set lines=36 columns=84 linespace=0
set guioptions-=T  " no toolbar
"set guioptions-=L " no menubar
"set guioptions-=l
set guicursor=n:blinkon0
if get(g:conf, 'posh_font', 0)
    set guifont=Ubuntu\ Mono\ for\ Powerline:h24
endif

