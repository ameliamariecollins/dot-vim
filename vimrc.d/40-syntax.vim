scriptencoding utf-8

if !has('syntax')
    finish
endif

syntax on

" Load indentation rules and plugins according to the detected filetype.
filetype plugin indent on

"    if v:version >= 703
"        set colorcolumn=81
"    endif
if get(g:conf, 'posh_font', 0)
    set listchars=nbsp:␣,tab:├─,trail:▪,eol:¶,precedes:,extends:
    try
	set listchars+=space:·
    catch
    endtry
else
    set listchars=nbsp:#,tab:\|_,trail:.,eol:$,precedes:<,extends:>
    try
	set listchars+=space:.
    catch
    endtry
endif

execute 'colorscheme' get(g:conf, 'color_scheme', 'helix')

" Satisfy $() for modern shells including ash
" if has("autocmd")
"     augroup sh_syntax
" 	autocmd!
" 	autocmd Syntax sh syn region shCommandSub
" 	    \ matchgroup=shCmdSubRegion
" 	    \ start="\$("  skip='\\\\\|\\.' end=")"
" 	    \ contains=@shCommandSubList
" 	autocmd Syntax sh syn region shArithmetic
" 	    \ matchgroup=shArithRegion
" 	    \ start="\$((" skip='\\\\\|\\.' end="))"
" 	    \ contains=@shArithList
"     augroup END
" endif

if has('eval')
    let g:vim_indent_cont = &sw
endif

" For highlighted numbers:
"    let python_highlight_numbers = 1
" For highlighted builtin functions:
"    let python_highlight_builtins = 1
" For highlighted standard exceptions:
"    let python_highlight_exceptions = 1
" Highlight erroneous whitespace:
"    let python_highlight_space_errors = 1
" If you want all possible Python highlighting (the same as setting the
" preceding options):
"    let python_highlight_all = 1
let python_highlight_all = 1

"match PythonOverLength /\%81v.*/
"match OverLength /\%81v.*/
"match trailingSpace /\s\+$/

