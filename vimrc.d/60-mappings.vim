scriptencoding utf-8

"-- Leader-based mappings
"let mapleader = ","
nnoremap <Leader>1 :1b<CR>
nnoremap <Leader>2 :2b<CR>
nnoremap <Leader>3 :3b<CR>
nnoremap <Leader>4 :4b<CR>
nnoremap <Leader>5 :5b<CR>
nnoremap <Leader>6 :6b<CR>
nnoremap <Leader>7 :7b<CR>
nnoremap <Leader>8 :8b<CR>
nnoremap <Leader>9 :9b<CR>
nnoremap <Leader>0 :10b<CR>
nnoremap <leader>p "+p
nnoremap <leader>P "+P
nnoremap <leader>i :setlocal list!<CR>
nnoremap <leader>h :set hlsearch!<CR>
nnoremap <leader>s :set spell!<CR>
"nnoremap <leader>v :e $MYVIMRC<CR>

if has('user-commands')
    command! W w
endif

if has('eval')
    map <F9> :%TOhtml<CR>ZZ
endif

"map <F7> :make<CR>
map Q gq

" More intuitive cursor movement for line-wrap mode:
map <C-Up> g<Up>
map <C-Down> g<Down>
imap <C-Up> <C-[> g<Up> i
imap <C-Down> <C-[> g<Down> i
map <leader>y "+y

" Execute file being edited with <Shift> + e:
"map <buffer> <S-e> :w<CR>:!/usr/bin/env python % <CR><CR>

" Suppress Ctrl-C warning
nnoremap <C-c> <silent> <C-c>

