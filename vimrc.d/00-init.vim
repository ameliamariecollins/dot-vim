scriptencoding utf-8

set nocompatible

let s:vimrc_local = expand('~/.vim/vimrc.local')
if filereadable(s:vimrc_local)
    execute 'source' s:vimrc_local
endif

