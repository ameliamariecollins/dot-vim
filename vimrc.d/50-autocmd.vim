scriptencoding utf-8

if !has("autocmd")
    finish
endif

if has('syntax')
    augroup Syntax
	au BufNewFile,BufEnter,BufRead,InsertLeave * syn sync fromstart
    augroup END
endif

augroup Text
    au FileType text setlocal textwidth=70
augroup END

"au BufNewFile,BufRead *.ini set ft=ini
au BufNewFile,BufRead *.s set ft=asm
au BufNewFile,BufRead *.py set ft=python colorcolumn=80
au BufNewFile,BufRead CMakefile,cmakefile set ft=cmake colorcolumn=80
au BufNewFile,BufRead *.h,*.c,*.cpp,*.cc set colorcolumn=80
au BufNewFile,BufRead *.f,*.f95,*.f03,*.f08,*.f15 set colorcolumn=80
au BufNewFile,BufRead *.hs set et colorcolumn=80 et
" Lazy redraw quells redraws during macro execution. Unfortunately, This
" breaks some plug-ins that do an initial draw during startup, such as
" Lightline. However, if autocmd facility is present, it can be used to
" help the initial draw.
set lazyredraw
autocmd VimEnter * redraw

augroup TeX
    autocmd Filetype tex setlocal tw=75 colorcolumn=80
augroup ENDPlug 'cdelledonne/vim-cmake'

autocmd FileType c setlocal commentstring=//\ %s

augroup Python
    autocmd FileType python setlocal expandtab ts=8 sts=4 sw=4 formatoptions=croql
    autocmd FileType python hi OverLength guibg=#301010
    autocmd FileType python match OverLength /\%81v.*/
    autocmd FileType python match TrailingSpace /\s\+$/
    au FileType python nnoremap <buffer> <leader>b :Black target_version=py311<CR>
augroup END

augroup Rust
    au!
    autocmd FileType rust nnoremap <buffer><Leader>f :RustFmt<CR>
    autocmd FileType rust vnoremap <buffer><Leader>f :RustFmtRange<CR>
augroup END

augroup VimWiki
    au FileType vimwiki nnoremap <buffer> <leader>wha :VimwikiAll2HTML<CR>
"     "map <leader>w* :VimwikiAll2HTML<CR>
"     "map <leader>w. :Vimwiki2HTML<CR>
augroup END


au BufRead,BufNewFile *.md
	    \ set wrap linebreak nolist textwidth=0 wrapmargin=0

augroup Mako
    au BufNewFile,BufRead,BufEnter *.mako set ft=mako
    autocmd FileType mako setlocal expandtab sw=4 indentexpr=""
augroup END

augroup Nginx
    autocmd BufWritePost */nginx.conf call g:ReloadNginx()
augroup END

function! g:ReloadNginx()
    if "$USER" == 'root'
	silent execute ':!nginx -s reload'
    else
	silent execute ':!sudo nginx -s reload'
	redraw
    endif
endfunction

