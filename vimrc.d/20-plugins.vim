scriptencoding utf-8

let s:plug_vim = expand('~/.vim/autoload/plug.vim')
if !filereadable(s:plug_vim)
    silent execute '!curl -sLo' s:plug_vim '--create-dirs ' .
	\ 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    execute 'source' s:plug_vim
    let s:trigger_pluginstall = 1
endif
"
call plug#begin('~/.vim/plugged')
Plug 'junegunn/vim-plug'

"-- Vim Behavior
Plug 'tpope/vim-surround'
Plug 'tpope/vim-characterize'
Plug 'mg979/vim-visual-multi'	" Multiple cursors
Plug 'farmergreg/vim-lastplace'	" Reopen files in the last edit position
"Plug 'bling/vim-bufferline'		" show the list of buffers in the command bar
Plug 'wellle/targets.vim'		" various text objects offer more targets
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'tomtom/tcomment_vim'
"Plug 'tpope/vim-commentary'
Plug 'chrisbra/unicode.vim'		" Unicode and digraphs

Plug 'mivok/vimtodo'

Plug 'lifepillar/pgsql.vim'
let g:sql_type_default = 'pgsql'
" Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
"Plug 'tpope/vim-fugitive'   " Git support
Plug 'arcticicestudio/nord-vim'
Plug 'sainnhe/artify.vim'
Plug 'albertomontesg/lightline-asyncrun' " Integration of https://github.com/skywind3000/asyncrun.vim
Plug 'rmolin88/pomodoro.vim'
set laststatus=2
set noshowmode

"--- Lightline
let g:lightline = get(g:, 'lightline', {})
let g:lightline.enable = { 'statusline': 1, 'tabline': 1 }
let g:lightline.colorscheme = 'rosepine'
let g:lightline.component = get(g:lightline, 'component', {})
let g:lightline.component_visible_condition =
    \ get(g:lightline, 'component_visible_condition', {})
if get(g:conf, 'posh_font', 0)
    let g:lightline.component.readonly = 
	\ '%{&ft == "help" ? "" : &ro ? "¤" : ""}'
    let g:lightline.component_visible_condition.readonly =
	\ '(&ft != "help" && &ro)'

    let g:lightline.component.modified =
	\ '%{&ft == "help" ? "" : &mod ? "⦿" : &ma ? "" : "-"}'
    let g:lightline.component_visible_condition.modified =
	\ '(&ft != "help" && (&mod || !&ma))'

    let g:lightline.separator = {
	\   'left':  "\ue0bc",
	\   'right': "\ue0ba"
	\ }
    let g:lightline.subseparator = {
	\   'left':  "\ue0bb",
	\   'right': "\ue0bb"
	\ }
endif
function! LightlineFugitive() abort
    if !exists('*fugitiveHead')
	return ''
    endif
    let branch = fugitiveHead()
    return branch == '' ? '' : '⭠ ' . branch
endfunction
let g:lightline.component.fugitive = '%{LightlineFugitive()}'
let g:lightline.component_visible_condition.fugitive =
    \ '(LightlineFugitive() != "")'
let g:lightline.active = {
    \ 'left': [ [ 'mode', 'paste' ],
    \         [ 'fugitive', 'readonly', 'filename', 'modified' ] ]
    \ }
Plug 'itchyny/lightline.vim'

"--- tmux
let g:tmuxline_preset = 'nightly_fox'
Plug 'edkolev/tmuxline.vim'
"Plug 'christoomey/vim-tmux-navigator'

Plug 'ryanoasis/vim-devicons'

let g:TerminusCursorShape=0
Plug 'wincent/terminus'

"=== Color schemes
"Plug 'godlygeek/csapprox'		# 256-color themes on lesser displays
Plug 'lifepillar/vim-colortemplate'
Plug 'flazz/vim-colorschemes'
Plug 'https://gitlab.com/ameliamariecollins/vim-colorscheme-helix.git'
" Plug 'catppuccin/vim', { 'as': 'catppuccin' }

"=== Support for external software
let g:languagetool_jar = '/opt/homebrew/opt/languagetool/libexec/languagetool-commandline.jar'
" Plug 'vim-scripts/LanguageTool'
"Plug 'dpelle/vim-LanguageTool'
Plug 'lervag/vimtex'	    " TeX support
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
"-- ctags
Plug 'majutsushi/tagbar', { 'on': 'TagbarToggle' }
nnoremap <leader>t :TagbarToggle<CR>
"-- Signify: Show vcs diffs as symbols in left columns
Plug 'mhinz/vim-signify'
" default updatetime 4000ms is not good for async update
set updatetime=100

"=== Support for file formats
"Plug 'python-mode/python-mode'
Plug 'lifepillar/vim-cheat40'
Plug 'thinca/vim-befunge'
Plug 'habamax/vim-rst'
"Plug 'darfink/vim-plist'
" Plug 'lacygoill/vim9-syntax'
Plug 'lesliev/vim-inform7'
Plug 'chrisbra/csv.vim'		" CSV file support
Plug 'chr4/nginx.vim'
"Plug 'shmargum/vim-sass-colors'
Plug 'sophacles/vim-bundle-mako'
let g:haskell_conceal_wide = 1
let g:haskell_conceal_bad = 1
Plug 'dag/vim2hs'
Plug 'JuliaEditorSupport/julia-vim'

"Plug 'vim-scripts/gtk-vim-syntax'
Plug 'bfrg/vim-cpp-modern'
Plug 'cespare/vim-toml'
Plug 'wgwoods/vim-systemd-syntax'
"Plug 'cdelledonne/vim-cmake'
"Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'prabirshrestha/vim-lsp'
if executable('rust-analyzer')
  au User lsp_setup call lsp#register_server({
        \   'name': 'Rust Language Server',
        \   'cmd': {server_info->['rust-analyzer']},
        \   'whitelist': ['rust'],
        \ })
endif

Plug 'rust-lang/rust.vim'

let g:ale_python_auto_virtualenv = 1
" let g:ale_linters = {
"     \ 'python': ['flake8', 'pydocstyle', 'bandit'],
"     \ 'c': ['clang'],
"     \ 'cpp': ['clang', 'g++'],
"     \ 'rust': ['analyzer'],
"     \ }
    " \ 'rust': ['analyzer'],
" let g:ale_rust_cargo_use_clippy = executable('cargo-clippy')
" let g:ale_fixers = {
"     \ 'rust': ['rustfmt', 'trim_whitespace', 'remove_trailing_lines']
"     \ }
" let g:ale_fixers = {
"     \ 'cpp': ['clang-format'],
"     \ 'go': ['gofmt'],
"     \ }
" let g:ale_linters_ignore = {'cpp': ['clang-check', 'clangtidy']}
let g:ale_fix_on_save = 1
let g:ale_python_flake8_options = '--max-line-length=99'
set completeopt=menu,menuone,preview,noselect,noinsert
let g:ale_completion_enabled = 1
Plug 'dense-analysis/ale'

let g:rustfmt_autosave = 1
let g:rustfmt_emit_files = 1
let g:rustfmt_fail_silently = 0

"--- YouCompleteMe
let g:ycm_confirm_extra_conf = 0
Plug 'ycm-core/YouCompleteMe'

"--- Clang Format
" let g:clang_format#style_options = {
" 	    \ "Standard" : "C11"
"     \ }
" For info on styles: clang-format -dump-config
Plug 'rhysd/vim-clang-format'
autocmd FileType c,cpp,objc nnoremap <buffer><Leader>f :<C-u>ClangFormat<CR>
autocmd FileType c,cpp,objc nnoremap <buffer><Leader>F :<C-u>ClangFormatAutoToggle<CR>
autocmd FileType c,cpp,objc vnoremap <buffer><Leader>f :ClangFormat<CR>

"-- Black
" let g:black_use_virtualenv = 1
" let g:black_linelength = 79
Plug 'psf/black', { 'branch': 'stable' }
"Plug 'EgZvor/black'

Plug 'junegunn/goyo.vim'	" Distraction-free writing

"Plug 'vimwiki/vimwiki'
"let g:vimwiki_list = [ {
    "\    'path':'~/.vim-wiki',
    "\    'path_html':'~/.vim-wiki/export/html',
    "\ } ]

"Plug 'nathanaelkane/vim-indent-guides'
Plug 'christoomey/vim-tmux-navigator'
Plug 'mbbill/undotree'
noremap <leader>ut :UndotreeToggle<CR>

Plug 'itchyny/screensaver.vim'

Plug 'preservim/nerdtree'
Plug 'PhilRunninger/nerdtree-buffer-ops'
Plug 'PhilRunninger/nerdtree-visual-selection'
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

" MarkDown
Plug 'godlygeek/tabular'
" let g:vim_markdown_folding_disabled = 1 " or set [no]foldenable
" let g:vim_markdown_no_default_key_mappings = 1
" let g:vim_markdown_conceal = 0 " or set conceallevel=2
Plug 'plasticboy/vim-markdown'

"Plug 'jewes/Conque-Shell'
"Plug 'othree/html5.vim'

"Plug 'kana/vim-textobj-user'
"Plug 'glts/vim-textobj-comment'

call plug#end()

if exists('s:trigger_pluginstall')
    execute 'PlugInstall'
endif

