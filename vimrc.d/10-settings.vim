scriptencoding utf-8

let g:conf = get(g:, 'conf', {}) " Ensure presence of conf dictionary

set directory=~/.vim-swp//,~/tmp//,/var/tmp//,.

set ttyfast

"set cmdheight=1 "The commandbar height
set laststatus=2
"set statusline=%<%f%h\ %m%r%=(%b\ 0x%B)\ \ %l,%c%V\ %P

set showcmd	" Show (partial) command in status line.
set showmatch	" Show matching brackets.
set ignorecase	" Do case insensitive matching,
set smartcase   "     but honor case if caps appear
set incsearch	" Incremental search
set autowrite	" Automatically save before commands like :next and :make
"-- Searching
set nohlsearch          " highlight matches

set errorbells visualbell

set textwidth=0		" Don't wrap words by default
set sw=4        " Change indent levels to 4 spaces.
"set tabstop=4  " Display tabs as four spaces. Somewhat misleading.
set tabstop=8  " Display tabs as eight spaces.
set softtabstop=4  " Natural feel for tab entry in editing mode, but make it 4 spaces
"set expandtab  " Make spaces into tabs?
set modeline modelines=7 modelineexpr

set omnifunc=syntaxcomplete#Complete
set wildmenu wildmode=longest:full,full
"set cpo-=<
set wcm=<C-Z>
"set cino=(0

set timeout timeoutlen=2000 ttimeoutlen=100

set history=100

if $TERM == 'linux'
    let g:conf.posh_font = 0
    set t_Co=16
endif

let g:netrw_home=expand($HOME)

if has('persistent_undo')
    if exists('g:conf.vim_undo_dir') && has('eval')
	let s:vim_undo_dir = expand(g:conf.vim_undo_dir)
	call oxime#ensure_dir(g:conf.vim_undo_dir)
	execute 'set undodir=' . g:conf.vim_undo_dir
    endif
    set undofile
    set undolevels=100
    set undoreload=10000
endif

if filereadable('/usr/bin/fc-cache')
    let s:fonts_dir = expand('~/.fonts')
    let s:powerline_font = 'ubuntu-mono-powerline-ttf'
    let s:powerline_font_dir = s:fonts_dir . '/' . s:powerline_font

    call oxime#ensure_dir(s:fonts_dir)
    if !isdirectory(s:powerline_font_dir)
	silent! execute '!git clone -q https://github.com/pdf/'
	    \ . s:powerline_font .  '.git' s:powerline_font_dir
	silent! execute '!fc-cache -f' s:fonts_dir
    endif
endif

