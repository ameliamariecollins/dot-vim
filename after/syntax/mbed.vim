
syn keyword mbedExtraFunction time

syn keyword mbedExtraSpecialFunction callback

syn keyword mbedExtraTypedef SPI I2C Thread InterruptIn DigitalIn DigitalOut DigitalInOut ConditionVariable
    \ EventFlags Idle loop Mail Mutex Queue Semaphore ThisThread Thread Event
    \ EventQueue UserAllocatedEvent BufferedSerial UnbufferedSerial QuadSPI QSPI SPI
    \ SPISlave AnalogIn AnalogOut BusIn BusOut BusInOut DigitalIn DigitalOut
    \ DigitalInOut InterruptIn PortIn PortOut PortInOut PwmOut USBAudio USBCDC
    \ USBCDC_ECM USBHID USBKeyboard USBMIDI USBMouse USBMouseKeyboard USBMSD
    \ USBSerial CAN FlashIAP I2C I2CSlave MbedCRC ResetReason RTC Ticker Time
    \ Timeout Timer Wait DeepSleepLock LowPowerTicker LowPowerTimeout LowPowerTimer
    \ mbed_mem_trace mpug_mgmt MemoryPool mbed_stats Assert ATCmdParser Callback
    \ CircularBuffer CriticalSectionLock Debug Error handling FileHandle NonCopyable
    \ PlatformMutex Poll ScopedRamExecutionLock ScopedRomWriteLock SharedPtr Span
    \ Dir FATFileSystem File FileSystem KVStore kvstore_global_api LittleFileSystem
    \ BlockDevice BufferedBlockDevice ChainingBlockDevice DataFlashBlockDevice
    \ FlashIAPBlockDevice FlashSimBlockDevice HeapBlockDevice MBRBlockDevice
    \ ProfilingBlockDevice QSPIFBlockDevice SDBlockDevice SlicingBlockDevice
    \ SPIFBlockDevice CellularNonIPSocket Socket SocketAddress SocketStats TCPSocket
    \ UDPSocket Cellular Ethernet Mesh DTLSSocket TLSSocket DNS API BLE GAP
    \ GattClient GattServer SecurityManager MessageBuilder MessageParser
    \ NFCController NFCEEPROM SimpleMessageParser LoRaWANInterface LoRaRadio
    \ DeviceKey

hi def link mbedExtraFunction Function
hi def link mbedExtraSpecialFunction Title
hi def link mbedExtraTypedef Type
"hi def link cExtraConstant Constant
"hi def link cExtraStruct Type
"hi def link cExtraMacro Macro
"hi def link cExtraEnum Type
"hi def link cExtraDefine Constant
