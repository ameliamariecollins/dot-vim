runtime! syntax/zmq.vim
runtime! syntax/mbed.vim

syn keyword cExtraFunction calloc free realloc reallocf valloc aligned_alloc
    \ exit abort memset malloc printf perror getline fopen fclose strlcpy
    \ strcpy strncpy memcpy strdup strndup strcmp strncmp stricmp strlen 
    \ strnlen atrcasesmp strncasecmp puts sprintf snprintf set_time time

syn keyword cExtraTypedef bool tm time_t boolean PinName Thread InterruptIn DigitalIn
    \DigitalOut DigitalInOut
syn keyword cExtraSpecialFunction main
syn match cExtraTodo "\<\(OUCH\|BOOM\|CRASH\|DANGER\|WARNING\|NOTE\|Y2K\)\>!*"
    \ containedin=.*Comment,.*CommentL,vimCommentTitle
"syn keyword cExtraTodo contained OUCH

hi def link cExtraFunction Function
hi def link cExtraSpecialFunction Title
hi def link cExtraTypedef Type
hi def link cExtraTodo Todo
"hi def link cExtraConstant Constant
"hi def link cExtraStruct Type
"hi def link cExtraMacro Macro
"hi def link cExtraEnum Type
"hi def link cExtraDefine Constant
