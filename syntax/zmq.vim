
syn keyword czmqFunction zactor_new zactor_destroy zactor_send zactor_recv
 \ zactor_is zactor_resolve zactor_test zauth_new zauth_allow zauth_deny
 \ zauth_configure_plain zauth_configure_curve zauth_configure_gssapi
 \ zauth_set_verbose zauth_destroy zauth_test zbeacon_new zbeacon_destroy
 \ zbeacon_hostname zbeacon_set_interval zbeacon_noecho zbeacon_publish
 \ zbeacon_silence zbeacon_subscribe zbeacon_unsubscribe zbeacon_socket
 \ zbeacon_test zcert_new zcert_new_from zcert_destroy zcert_public_key
 \ zcert_secret_key zcert_public_txt zcert_secret_txt zcert_set_meta
 \ zcert_meta zcert_meta_keys zcert_load zcert_save zcert_save_public
 \ zcert_save_secret zcert_apply zcert_dup zcert_eq zcert_fprint zcert_print
 \ zcert_test zcertstore_new zcertstore_destroy zcertstore_lookup
 \ zcertstore_insert zcertstore_fprint zcertstore_print zcertstore_test
 \ zchunk_new zchunk_destroy zchunk_resize zchunk_size zchunk_max_size
 \ zchunk_data zchunk_set zchunk_fill zchunk_append zchunk_consume
 \ zchunk_exhausted zchunk_read zchunk_write zchunk_slurp zchunk_dup
 \ zchunk_fprint zchunk_print zchunk_is zchunk_test zclock_sleep zclock_time
 \ zclock_mono zclock_timestr zclock_test zclock_log zconfig_new
 \ zconfig_destroy zconfig_name zconfig_value zconfig_put zconfig_set_name
 \ zconfig_set_value zconfig_child zconfig_next zconfig_locate zconfig_resolve
 \ zconfig_set_path zconfig_at_depth zconfig_execute zconfig_set_comment
 \ zconfig_comments zconfig_load zconfig_save zconfig_filename zconfig_reload
 \ zconfig_chunk_load zconfig_chunk_save zconfig_has_changed zconfig_fprint
 \ zconfig_print zconfig_test zctx_new zctx_destroy zctx_shadow
 \ zctx_set_iothreads zctx_set_linger zctx_set_pipehwm zctx_set_sndhwm
 \ zctx_set_rcvhwm zctx_underlying zctx_test zctx__socket_new
 \ zctx__socket_pipe zctx__socket_destroy zdigest_new zdigest_destroy
 \ zdigest_update zdigest_data zdigest_size zdigest_string zdigest_test
 \ zdir_new zdir_destroy zdir_path zdir_modified zdir_cursize zdir_count
 \ zdir_flatten zdir_flatten_free zdir_remove zdir_diff zdir_resync zdir_cache
 \ zdir_fprint zdir_print zdir_test zdir_patch_new zdir_patch_destroy
 \ zdir_patch_dup zdir_patch_path zdir_patch_file zdir_patch_op
 \ zdir_patch_vpath zdir_patch_digest_set zdir_patch_digest zdir_patch_test
 \ zfile_new zfile_destroy zfile_dup zfile_filename zfile_restat
 \ zfile_modified zfile_cursize zfile_is_directory zfile_is_regular
 \ zfile_is_readable zfile_is_writeable zfile_is_stable zfile_has_changed
 \ zfile_remove zfile_input zfile_output zfile_read zfile_write zfile_close
 \ zfile_handle zfile_digest zfile_test zfile_exists zfile_delete zfile_stable
 \ zfile_mode_private zfile_mode_default zframe_new zframe_new_empty
 \ zframe_destroy zframe_recv zframe_send zframe_size zframe_data zframe_dup
 \ zframe_strhex zframe_strdup zframe_streq zframe_more zframe_set_more
 \ zframe_eq zframe_reset zframe_print zframe_is zframe_test
 \ zframe_recv_nowait zframe_fprint zgossip zgossip_test zhash_new
 \ zhash_destroy zhash_insert zhash_update zhash_delete zhash_lookup
 \ zhash_rename zhash_freefn zhash_size zhash_dup zhash_keys zhash_first
 \ zhash_next zhash_cursor zhash_comment zhash_save zhash_load zhash_refresh
 \ zhash_autofree zhash_pack zhash_unpack zhash_foreach zhash_test zlist_new
 \ zlist_destroy zlist_first zlist_next zlist_last zlist_head zlist_tail
 \ zlist_item
 \ zlist_append zlist_push zlist_pop zlist_remove zlist_freefn zlist_dup
 \ zlist_size zlist_sort zlist_autofree zlist_test zloop_new zloop_destroy
 \ zloop_reader zloop_reader_end zloop_reader_set_tolerant zloop_poller
 \ zloop_poller_end zloop_poller_set_tolerant zloop_timer zloop_timer_end
 \ zloop_set_verbose zloop_start zloop_test zmonitor_new zmonitor_destroy
 \ zmonitor_recv zmonitor_socket zmonitor_set_verbose zmonitor_test zmsg_new
 \ zmsg_destroy zmsg_recv zmsg_send zmsg_size zmsg_content_size zmsg_prepend
 \ zmsg_append zmsg_pop zmsg_pushmem zmsg_addmem zmsg_pushstr zmsg_addstr
 \ zmsg_pushstrf zmsg_addstrf zmsg_popstr zmsg_remove zmsg_first zmsg_next
 \ zmsg_last zmsg_save zmsg_load zmsg_encode zmsg_decode zmsg_dup zmsg_print
 \ zmsg_is zmsg_test zmsg_unwrap zmsg_recv_nowait zmsg_wrap zmsg_push zmsg_add
 \ zmsg_fprint zmutex_new zmutex_destroy zmutex_lock zmutex_unlock
 \ zmutex_try_lock zmutex_test zpoller_new zpoller_destroy zpoller_add
 \ zpoller_remove zpoller_wait zpoller_expired zpoller_terminated zpoller_test
 \ zproxy_new zproxy_destroy zproxy_capture zproxy_pause zproxy_resume
 \ zproxy_test zrex_new zrex_destroy zrex_valid zrex_strerror zrex_hits
 \ zrex_eq zrex_hit zrex_test zsocket_new zsocket_destroy zsocket_bind
 \ zsocket_unbind zsocket_connect zsocket_disconnect zsocket_poll
 \ zsocket_type_str zsocket_sendmem zsocket_signal zsocket_wait zsocket_test
 \ zsock_new zsock_destroy
 \ zsock_new_ zsock_destroy_ zsock_new_pub_ zsock_new_sub_ zsock_new_req_
 \ zsock_new_rep_ zsock_new_dealer_ zsock_new_router_ zsock_new_push_
 \ zsock_new_pull_ zsock_new_xpub_ zsock_new_xsub_ zsock_new_pair_
 \ zsock_new_stream_ zsock_bind zsock_unbind zsock_connect zsock_disconnect
 \ zsock_attach zsock_type_str zsock_send zsock_recv zsock_set_unbounded
 \ zsock_signal zsock_wait zsock_is zsock_resolve zsock_test zsock_monitor_new
 \ zsock_monitor_destroy zsock_monitor_recv zsock_monitor_set_verbose
 \ zsock_monitor_listen zsock_monitor_start zsock_monitor_handle
 \ zsock_monitor_test zstr_recv zstr_send zstr_sendm zstr_sendf zstr_sendfm
 \ zstr_sendx zstr_recvx zstr_free zstr_test zstr_recv_nowait zsys_init
 \ zsys_socket zsys_close zsys_handler_set zsys_handler_reset
 \ zsys_catch_interrupts zsys_file_exists zsys_file_size zsys_file_modified
 \ zsys_file_mode zsys_file_delete zsys_file_stable zsys_dir_create
 \ zsys_dir_delete zsys_file_mode_private zsys_file_mode_default zsys_version
 \ zsys_sprintf zsys_vprintf zsys_udp_new zsys_udp_close zsys_udp_send
 \ zsys_udp_recv zsys_socket_error zsys_hostname zsys_daemonize zsys_run_as
 \ zsys_has_curve zsys_set_io_threads zsys_set_max_sockets zsys_socket_limit
 \ zsys_set_linger zsys_set_sndhwm zsys_set_rcvhwm zsys_set_pipehwm
 \ zsys_pipehwm zsys_set_interface zsys_interface zsys_set_logident
 \ zsys_set_logstream zsys_set_logsender zsys_set_logsystem zsys_error
 \ zsys_warning zsys_notice zsys_info zsys_debug zsys_test zthread_new
 \ zthread_fork zthread_test zuuid_new zuuid_destroy zuuid_data zuuid_size
 \ zuuid_str zuuid_set zuuid_set_str zuuid_export zuuid_eq zuuid_neq zuuid_dup
 \ zuuid_test

syn keyword czmqTypedef zactor_fn zactor_t zauth_t zbeacon_t
 \ zcertstore_t zcert_t zchunk_t zconfig_t zctx_t zdigest_t zdir_patch_t
 \ zdir_t zfile_t zframe_t zhash_t zlist_t zloop_t zmonitor_t zmsg_t zmutex_t
 \ zpoller_t zproxy_t zrex_t zsock_monitor_t zsock_t zsys_handler_fn
 \ zthread_detached_fn zuuid_t

syn keyword czmqConstant ZFRAME_MORE ZFRAME_REUSE ZFRAME_DONTWAIT ZFRAME_FINAL

"syn keyword czmqStruct GUdevClient GUdevClientClass GUdevDevice GUdevDeviceClass GUdevEnumerator GUdevEnumeratorClass

"syn keyword czmqMacro G_UDEV_CLIENT G_UDEV_CLIENT_CLASS G_UDEV_CLIENT_GET_CLASS G_UDEV_DEVICE G_UDEV_DEVICE_CLASS G_UDEV_DEVICE_GET_CLASS G_UDEV_ENUMERATOR G_UDEV_ENUMERATOR_CLASS G_UDEV_ENUMERATOR_GET_CLASS G_UDEV_IS_CLIENT G_UDEV_IS_CLIENT_CLASS G_UDEV_IS_DEVICE G_UDEV_IS_DEVICE_CLASS G_UDEV_IS_ENUMERATOR G_UDEV_IS_ENUMERATOR_CLASS

"syn keyword czmqEnum GUdevDeviceType

"syn keyword czmqDefine G_UDEV_TYPE_CLIENT G_UDEV_TYPE_DEVICE G_UDEV_TYPE_ENUMERATOR

" Default highlighting
hi def link czmqFunction Function
hi def link czmqTypedef Type
hi def link czmqConstant Constant
"hi def link czmqStruct Type
"hi def link czmqMacro Macro
"hi def link czmqEnum Type
"hi def link czmqDefine Constant


